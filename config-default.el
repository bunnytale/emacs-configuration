(defvar local-user-config
      #s(
         :data '(user-name "user"
                 user-email "<>"
                 emacs-config-dir ,user-emacs-directory
                 preferences-config-dir ,(concat "~/" "/.config/emacs")))
      "local user defined settings for emacs file editing operations")
