(defun org-roam-setup ()
  "setup org-roam with configuratins for proper use"
  (use-package "org-roam")

  (setq org-roam-directory "~/tasking/roam")
  (org-roam-db-autosync-mode)
  (setq org-roam-v2-ack t))

(provide 'org-roam-setup)
