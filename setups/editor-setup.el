    ;; this script setup editor configuration scheme

    (defun editor-after-load ()
    (add-to-list 'auto-mode-alist '("\\.tsx?$" . typescript-mode))

    (setq diary-file "~/tasking/diary")
    (add-hook 'calendar-mode-hook (lambda ()
                (set-face-background 'diary "gray")
                (set-face-foreground 'diary "black")))

  (setq org-default-notes-file  "~/tasking/notes.org")

  (add-to-list 'load-path (concat user-emacs-directory "/setups/") )

  (require 'org-roam-setup)
  (org-roam-setup)

  (require 'lisp-setup "my custom lisp setup environment" t))

(defun editor-font-config ()
  (set-face-background 'mode-line-inactive "default")
  (set-face-foreground 'mode-line-inactive "gray")

  (set-face-background 'mode-line "default")
  (set-face-foreground 'mode-line "white")

  (set-face-background 'tab-bar "default")
  (set-face-foreground 'tab-bar "gray")

  (set-face-background 'tab-bar-tab "default")
  (set-face-background 'tab-bar-tab-inactive "default")
  (set-face-foreground 'tab-bar-tab-inactive "light-black")
  (set-face-background 'tab-bar-tab-ungrouped "default")
  (set-face-background 'tab-bar-tab-group-inactive "default")
  (set-face-foreground 'tab-bar-tab "gray")

  (set-face-background 'tab-bar-tab-group-current "default")
  (set-face-background 'tab-line "default")

  ;; --> define editor keybindings
  (global-set-key (kbd "C-x C-1") #'delee-other-windows)
  (global-set-key (kbd "C-x C-2") #'split-window-below)
  (global-set-key (kbd "C-x C-3") #'split-window-right)
  (global-set-key (kbd "C-x C-4") #'delete-window)

  (global-set-key (kbd "C-x C-k C-t") #'kill-buffer))

;; this function enable most of editor configurations at once
(defun editor-setup ()

  (editor-font-config)

  ;; --> interface configuration
  (menu-bar-mode -1)
  (tool-bar-mode -1)

  (setq inhibit-startup-screen t)

  (use-package "evil"
    :ensure t
    :init
    (setq evil-undo-system 'undo-redo)
    (evil-mode t)
    (global-set-key (kbd "C-z") #'evil-emacs-state))

  (use-package "god-mode"
    :ensure t
    :config
    (setq god-mode-enable-function-key-translation t)
    :init
    (global-set-key (kbd "C-a") #'god-mode))

  (with-eval-after-load 'god-mode
    (evil-make-intercept-map god-local-mode-map 'normal)
    (add-hook 'god-local-mode-hook 'evil-normalize-keymaps))

  (size-indication-mode t)

  (require 'key-chord)
  (key-chord-mode 1)

  (setq-default tab-width 4)
  (setq-default indent-tabs-mode nil)

  (add-hook 'before-save-hook (lambda ()
                                (whitespace-cleanup)
                                (untabify)))

  (setq-default tab-always-indent 'complete)
  (editor-after-load))

(provide 'editor-setup)
