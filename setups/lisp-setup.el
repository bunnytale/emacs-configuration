;; ------------------------------------------------------
;;  this script configure my lisp editor configuration
;; ------------------------------------------------------

(defun lisp-mode-hook ()
  (make-variable-buffer-local 'show-paren-mode)
  (show-paren-mode 1)
  (set-face-background 'show-paren-match
                       (face-background 'default))
  (require 'paren)
  (if (boundp 'font-lock-comment-face)
      (set-face-foreground 'show-paren-match
                           (face-foreground 'font-lock-comment-face))
    (set-face-foreground 'show-paren-match
                         (face-foreground 'default)))
  (set-face-attribute 'show-paren-match nil :weight 'extra-bold)

  (set-face-foreground 'show-paren-match (face-background 'default))
  (set-face-foreground 'show-paren-match "#def"))

(defun setup-lisp-mode ()
  (setq show-paren-delay .5)
  (setq show-paren-style 'parenthesis)

  (add-hook 'lisp-mode-hook 'lisp-mode-hook))

(provide 'setup-lisp-mode)
