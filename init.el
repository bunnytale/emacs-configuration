;; add setups directory to load pathh
(add-to-list 'load-path (concat user-emacs-directory "./setups/"))

;; encoding setup
(prefer-coding-system 'utf-8)
(set-default-coding-systems 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)

;; ----------------------------------------------------
;;  setup package repositories related configurations
;; ----------------------------------------------------

(require 'package)
(setq package-enable-at-startup nil)
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/"))
(add-to-list 'package-archives '("gnu"   . "http://elpa.gnu.org/packages/"))
(package-initialize)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(eval-when-compile
  (require 'use-package))

(eval-when-compile
  (if (file-exists-p "~/.config/emacs/config.el")
      (load "~/.config/emacs/config")
    (load (concat user-emacs-directory "./config-default"))))

(setq backup-directory-alist
      `(("." . ,(concat "~/.local/share/emacs/" "/backups/"))))
(setq auto-save-file-name-transform
      `(".*" ,(concat "~/.local/share/emacs/" "./backups/") t))

(require 'editor-setup)
(add-hook 'after-init-hook 'editor-setup)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   '(cmake-mode elixir-mode org-ref markdown-mode org-roam php-mode handlebars-mode rust-mode scala-mode typescript-mode org-brain :evil key-chord coffee-mode god-mode go-mode evil use-package))
 '(warning-suppress-types '((org-roam) (org-roam) (org-roam) (org-roam) (org-roam))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
